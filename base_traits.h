//===----------------------------------------------------------------------===//
//  (C) Copyright:  Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//

#pragma once

/*
Used compiler intrinsics
__record_base_count(T) -> size_t            //number of all bases (no duplicates)
__record_base_type(T) -> Type

__record_direct_base_count(T) -> size_t		//number of direct bases // class C : A, B {}  will return  2
__record_direct_base_type(T,I) -> Type		

__record_virtual_base_count(T) -> size_t
__record_virtual_base_type(T,I) -> Type

__record_base_access_spec(T,I) -> size_t flags (private, public)	//TODO 
__record_base_is_virtual(T,I) -> bool								//TODO 
*/

namespace meta {

/// bases (all bases?)
/*
If T is a class, the member typedef type shall be a tuple whose types are all base classes 
of T in the order specified for initializing bases in 20.6.2.  (Not the same!)
If T is not a class, then the member typedef type shall be a tuple with zero arguments
*/
template<class R, class IdxSeq>
struct bases_impl;

template<class R, size_t... Idx>
struct bases_impl<R, std::index_sequence<Idx...>>
{
	typedef std::tuple<__record_base_type(R, Idx)... >  type;
};
template<class R>
struct bases
	: public bases_impl<R, std::make_index_sequence< __record_base_count(R) >>
{
	enum { count = __record_base_count(R) };
};


/// direct_bases
/*
If T is a class, the member typedef type shall be a tuple whose types are all direct base classes 
of T in the order specified for initializing bases in 20.6.2. 
If T is not a class, then the member typedef type shall be a tuple with zero arguments
*/
template<class R, class IdxSeq>
struct direct_bases_impl;

template<class R, size_t... Idx>
struct direct_bases_impl<R, std::index_sequence<Idx...>> 
{
	typedef std::tuple<__record_direct_base_type(R, Idx)... >  type;
};

template<class R>
struct direct_bases 
	: public direct_bases_impl<R, std::make_index_sequence< __record_direct_base_count(R) >>
{
	enum { count = __record_direct_base_count(R) };
};

/// virtual_bases
template<class R, class IdxSeq>
struct virtual_bases_impl;

template<class R, size_t... Idx>
struct virtual_bases_impl<R, std::index_sequence<Idx...>>
{
	typedef std::tuple<__record_virtual_base_type(R, Idx)... >  type;
};

template<class R>
struct virtual_bases
	: public virtual_bases_impl<R, std::make_index_sequence< __record_virtual_base_count(R) >>
{
	enum { count = __record_virtual_base_count(R) };
};

}; //namespace meta


//////////////////////////////////////////////////////////////////////////
namespace method_traits_test
{

class E {};
class D {};
class C : virtual public D, private E {};
class B : virtual public D, public E {};
class A : public B, public C {};

// problem with bases order and duplicate bases.
// It follows that bases<A>::type is tuple<B, C, D, E>
static_assert(std::is_same< meta::bases<A>::type, std::tuple<B, C, D, E> >::value, "wrong bases type!");
static_assert(std::tuple_size< meta::bases<A>::type >::value == 4, "wrong bases count!");
static_assert(meta::bases<A>::count == 4, "wrong bases count");

// Similarly, direct_bases<A>::type is tuple<B, C>
static_assert(std::is_same< meta::direct_bases<A>::type, std::tuple<B, C> >::value, "wrong direct_bases type!");
static_assert(meta::direct_bases<A>::count == 2, "wrong direct_bases count!");
static_assert(std::tuple_size< meta::direct_bases<A>::type >::value == 2, "wrong virtual_bases count!");

// Similarly, virtual_bases<A>::type is tuple<D>
static_assert(std::is_same< meta::virtual_bases<A>::type, std::tuple<D> >::value, "wrong virtual_bases type!");
static_assert(meta::virtual_bases<A>::count == 1, "wrong virtual_bases count!");
static_assert(std::tuple_size< meta::virtual_bases<A>::type >::value == 1, "wrong virtual_bases count!");

} //namespace method_traits_test
