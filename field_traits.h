//===----------------------------------------------------------------------===//
//  (C) Copyright:  Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//

#pragma once

/*
Used compiler intrinsics
__record_member_field_count(T) -> size_t
__record_member_field_type(T,I) -> Type
__record_member_field_identifier(T,I) -> string literal
__record_member_field_ptr(T,I) -> Ptr
__object_member_field_ref(T,Ref,I) -> Ref
__record_member_field_info(T,I) -> size_t (meta_info_enum)
__record_member_field_is_mutable(T,I) -> bool
__record_member_field_is_reference(T,I) -> bool
*/

namespace meta {

	template<class R>
	struct field_list_size {
		//static_assert(std::is_class<std::remove_cv<R>>::value 
		//		   || std::is_union<std::remove_cv<R>>::value, "R not class type");

		enum { value = __record_member_field_count(R) }; //int
	};

	template<class R, unsigned int I>
	struct field_identifier {
		//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
		static_assert(0 <= I && I < field_list_size<R>::value, "I out-of-bounds");

		static constexpr decltype(auto) value = __record_member_field_identifier(R, I); //string literal
	};

	template<class R, unsigned int I>
	struct field_type {
		//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
		static_assert(0 <= I && I < field_list_size<R>::value, "I out-of-bounds");

		typedef __record_member_field_type(R, I) type; //Type
	};

	template<class R, unsigned int I>
	using field_type_t = typename field_type<R, I>::type;

	template<class R, unsigned int I>
	struct field_info {
		//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
		static_assert(0 <= I && I < field_list_size<R>::value, "I out-of-bounds");

		static constexpr decltype(auto) info = __record_member_field_info(R, I);

		constexpr bool is_public() const    { return info & SPECS_PUBLIC; }
		constexpr bool is_private() const   { return info & SPECS_PRIVATE; }
		constexpr bool is_protected() const { return info & SPECS_PROTECTED; }
	};


	template<class R, class IdxSeq>
	struct field_data_impl;

	template<class R, size_t... Idx>
	struct field_data_impl<R, std::index_sequence<Idx...>>
	{
		//type of this record (class/struct/union)
		typedef		R		type;

		//how many member functions are int this record
		static constexpr size_t count = field_list_size<R>::value;

		//names of all non static member functions.
		static constexpr char const *names[] = { field_identifier<R, Idx>::value... };

		//all types 
		using types = std::tuple< __record_member_field_type(R, Idx)... >;  //abuse tuple as type list ???
	};

	template<class R, size_t... Idx>
	constexpr char const* field_data_impl<R, std::index_sequence<Idx...>>::names[];

	template<class R>
	struct field_data : public field_data_impl<R, std::make_index_sequence< field_list_size<R>::value >>
	{};

	template<class R, unsigned int I>
	auto field_ref(R& r) -> decltype(auto) {
		return __object_member_field_ref(R, r, I);
	};

	template<class R, unsigned int I>
	auto field_ref(const R& r) -> decltype(auto) {
		return __object_member_field_ref(R, r, I);
	};


}; //namespace meta


/// Convert class type into to tuple
template<typename R, std::size_t... I>
auto class2tuple_impl(const R& r, std::index_sequence<I...>) -> decltype(auto)
{
	return std::tuple< meta::field_type_t<R, I>... >(
		meta::field_ref<R, I>(r)...
		);
}

template<typename R, typename Indices = std::make_index_sequence< meta::field_list_size<R>::value > >
auto class2tuple(const R& r) -> decltype(auto)
{
	return class2tuple_impl(r, Indices());
}
