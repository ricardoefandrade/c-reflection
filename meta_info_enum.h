//===----------------------------------------------------------------------===//
//  (C) Copyright:  Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//

#pragma once

namespace meta {

	// should be in sync with the compiler (clang) enum!
	enum meta_info_enum
	{
		//access qualifiers
		SPECS_PUBLIC = (1 << 1),		//public access 
		SPECS_PRIVATE = (1 << 2),		//private access
		SPECS_PROTECTED = (1 << 3),		//protected access
		//qualifiers 
		SPECS_STATIC = (1 << 4),		//static
		SPECS_CONST = (1 << 5),			//const 
		SPECS_CONSTEXPR = (1 << 6),		//constexpr   	
		SPECS_VOLATILE = (1 << 7),		//volatile
		SPECS_RQ_LVALUE = (1 << 8),		// Ref Qual &
		SPECS_RQ_RVALUE = (1 << 9),		// Ref Qual &&
		SPECS_VIRTUAL = (1 << 10),		//virtual

		SPECS_FINAL = (1 << 11),		//final				 
		SPECS_VARIADIC = (1 << 12),		// ... 						
		SPECS_INLINE = (1 << 13),		//inline			

		SPECS_CONSTRUCTOR = (1 << 14),	//constructor		
		SPECS_DESTRUCTOR = (1 << 15),	//destructor			
		SPECS_CONVERSION = (1 << 16),	//int() operator		
		SPECS_OPERATOR = (1 << 17),		//operator +   		
		//Fields
		SPECS_MUTABLE = (1 << 18),		//mutable			
		SPECS_INSTANCE = (1 << 19),
		SPECS_BIT_FIELD = (1 << 20),

		SPECS_THROW = (1 << 21),		// throw
		SPECS_NOEXCEPT = (1 << 22),		// noexcept
		SPECS_PURE = (1 << 23),			// int fn() = 0;
		SPECS_DELETED = (1 << 24),		// = delete;
		SPECS_DEFAULTED = (1 << 25),	// = default;
	};

}; //namespace meta