//===----------------------------------------------------------------------===//
//  (C) Copyright:  Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//

#pragma once

/*
 Used compiler intrinsics
 __record_method_count(T) -> size_t    					number of member functions in class/struct T
 __record_method_type(T,I) -> Type						type of I'th member function in class/struct T
 __record_method_identifier(T,I) -> string				name of I'th member function in class/struct T
 __record_method_info(T,I) -> size_t (meta_info_enum)	binary ORed qualifiers values of I'th member function
 __record_function_param_identifier(T,I,P) -> string  	name of P'th parameter in I'th
*/

namespace meta {

template<class R>
struct method_list_size {
	//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");

	enum { value = __record_method_count(R) }; //int
};

template<class R, unsigned int I>
struct method_identifier {
	//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
	static_assert(0 <= I && I < method_list_size<R>::value, "I out-of-bounds");

	static constexpr decltype(auto) value = __record_method_identifier(R, I); //string literal
};

template<class R, unsigned int I>
struct method_type {
	//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
	static_assert(0 <= I && I < method_list_size<R>::value, "I out-of-bounds");

	typedef __record_method_type(R, I) type; //Type
};

template<class R, unsigned int I>
struct method_info {
	//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
	static_assert(0 <= I && I < method_list_size<R>::value, "I out-of-bounds");

	static constexpr decltype(auto) info = __record_method_info(R, I);

	static constexpr bool is_public() 			{ return (info & SPECS_PUBLIC); }
	static constexpr bool is_private() 			{ return (info & SPECS_PRIVATE); }
	static constexpr bool is_protected() 		{ return (info & SPECS_PROTECTED); }

	static constexpr bool is_const() 			{ return (info & SPECS_CONST); }
	static constexpr bool is_volatile() 		{ return (info & SPECS_VOLATILE); }
	static constexpr bool is_constexpr() 		{ return (info & SPECS_CONSTEXPR); }
	static constexpr bool is_ref_qual_lvalue() 	{ return (info & SPECS_RQ_LVALUE); }
	static constexpr bool is_ref_qual_rvalue() 	{ return (info & SPECS_RQ_RVALUE); }

	static constexpr bool is_variadic() 		{ return (info & SPECS_VARIADIC); }
	static constexpr bool is_inline() 			{ return (info & SPECS_INLINE); }

	static constexpr bool is_virtual() 			{ return (info & SPECS_VIRTUAL); }
	static constexpr bool is_final() 			{ return (info & SPECS_FINAL); }
	static constexpr bool is_pure() 			{ return (info & SPECS_PURE); }

	static constexpr bool is_constructor() 		{ return (info & SPECS_CONSTRUCTOR); }
	static constexpr bool is_destructor() 		{ return (info & SPECS_DESTRUCTOR); }
	static constexpr bool is_conversion() 		{ return (info & SPECS_CONVERSION); }
	static constexpr bool is_operator() 		{ return (info & SPECS_OPERATOR); }
};



template<class R, class IdxSeq>
struct method_data_impl;

template<class R, size_t... Idx>
struct method_data_impl<R, std::index_sequence<Idx...>>
{
	//type of this record (class/struct/union)
	typedef		R		type; 

	//all types 
	using types = std::tuple< __record_method_type(R, Idx)... >;  //abuse tuple as type list ???

	//how many member functions are int this record
	static constexpr size_t count = method_list_size<R>::value; 

	//names of all non static member functions.
	static constexpr char const *names[] = { method_identifier<R, Idx>::value... };

	//method_info
	static constexpr size_t infos[] = { method_info<R, Idx>::info... };
	//access info 
	static constexpr bool is_public(size_t index)		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_PUBLIC);
	}
	static  constexpr bool is_private(size_t index) 		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_PRIVATE);
	}
	static  constexpr bool is_protected(size_t index) 		{
		if (index >= count) return false;
		return (infos[index] & SPECS_PROTECTED);
	}
	//qualifiers info
	static constexpr bool is_const(size_t index) 			{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_CONST);
	}
	static constexpr bool is_volatile(size_t index) 		{
		if (index >= count) return false;
		return (infos[index] & SPECS_VOLATILE);
	}
	static constexpr bool is_constexpr(size_t index) 		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_CONSTEXPR);
	}
	static constexpr bool is_ref_qual_lvalue(size_t index) 	{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_RQ_LVALUE);
	}
	static constexpr bool is_ref_qual_rvalue(size_t index) 	{
		if (index >= count) return false;
		return (infos[index] & SPECS_RQ_RVALUE);
	}

	static  constexpr bool is_variadic(size_t index) 		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_VARIADIC);
	}
	static  constexpr bool is_inline(size_t index) 			{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_INLINE);
	}

	static  constexpr bool is_virtual(size_t index) 		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_VIRTUAL);
	}
	static  constexpr bool is_final(size_t index) 			{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_FINAL);
	}
	static  constexpr bool is_pure(size_t index) 			{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_PURE);
	}

	static  constexpr bool is_constructor(size_t index) 	{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_CONSTRUCTOR);
	}
	static  constexpr bool is_destructor(size_t index) 		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_DESTRUCTOR);
	}
	static  constexpr bool is_conversion(size_t index)		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_CONVERSION);
	}
	static  constexpr bool is_operator(size_t index) 		{ 
		if (index >= count) return false;
		return (infos[index] & SPECS_OPERATOR);
	}
};

template<class R, size_t... Idx>
constexpr char const* method_data_impl<R, std::index_sequence<Idx...>>::names[];

template<class R>
struct method_data : public method_data_impl<R, std::make_index_sequence< method_list_size<R>::value >>
{};

}; //namespace meta

