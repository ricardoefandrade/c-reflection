//===----------------------------------------------------------------------===//
//  (C) Copyright:  Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//

#pragma once

/*
namespace is not a type so we uses class that belong to the namespace to access it.
*/

/*
Used compiler intrinsics
__namespace_count(R) -> size_t
__namespace_type(R,I) -> Type
__namespace_identifier(R,I) -> string literal
*/

namespace meta {
	///TODO...
}; //namespace meta